//
//  UIGameViewController.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 19/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import UIKit
import Speech

class UIGameViewController: UIViewController,SFSpeechRecognizerDelegate {
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    var words: Array<String> = [""]
    var delayForDisappearing: TimeInterval = TimeInterval(0.3)
    
    
    @IBOutlet weak var textView: UILabel!
    @IBOutlet weak var hiddenButton: UIButton!
    @IBOutlet weak var correctView: UIImageView!
    @IBOutlet weak var wrongView: UIImageView!
    @IBOutlet weak var micView: UIImageView!
    
    var recognisedWord: String = String("")
    var expectedWord: String = String("")
    
    var index: Int = 0
    
    var correctAnswers: Int = 0
    var wrongAnswers: Int = 0
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        if(self.audioEngine.isRunning){
            self.micView.layer.removeAllAnimations()
            self.micView.alpha=0
            self.cancelRecording()
            self.hiddenButton.setTitle("Tap and I'll give you a word!", for: .normal)
            if(recognisedWord == expectedWord){
                self.correctAnswers+=1
                self.correctView.alpha = 1.0
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [],animations: {
                    self.correctView.alpha = 0
                })
            }else{
                self.wrongAnswers+=1
                self.wrongView.alpha = 1.0
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [],animations: {
                    self.wrongView.alpha = 0
                    
                })
                
            }
        }else{
            if(self.index < self.words.count){
                self.textView.alpha = 1.0
                self.textView.text = self.words[self.index]
                self.expectedWord = self.self.words[self.index]
                self.index = (self.index + 1)%words.count
                self.hiddenButton.setTitle("Tap after you've read the word", for: .normal)
                
                UIView.animate(withDuration: self.delayForDisappearing, animations: {self.textView.alpha = 0.0})
                
                DispatchQueue.global(qos: .userInitiated).async {
                    self.startRecord()
                }
                
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [.repeat,.autoreverse], animations: {self.micView.alpha=1})
                
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.correctView.alpha = 0
        self.wrongView.alpha = 0
        self.micView.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.words = WordsByComplexity.normalWords
        self.index = 0
        self.delayForDisappearing = TimeInterval(0.3)
    }
    
    func startRecord(){
        var resultFinal: String = ""
        request.requiresOnDeviceRecognition = true
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat){buffer, _ in
            self.request.append(buffer)
        }
        audioEngine.prepare()
        do { try audioEngine.start() } catch { print(error)}
        
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {
                resultFinal = result.bestTranscription.formattedString
                self.recognisedWord = resultFinal.lowercased()
            }
        })
    }
    
    func cancelRecording() {
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
        recognitionTask?.cancel()
    }
}
