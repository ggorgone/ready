//
//  UIAchievementsViewController.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 19/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import UIKit

class UIAchievementsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // Create achievements array and put data in it (just a dummy, the actual info must be retrived from a file)
    var achievements: [Achievement] =
        [Achievement(achieved: false, achievementDate: Date(), title: "The first step!", description: "Complete your first game session.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "High five! (Easy)", description: "Complete 5 game sessions on Easy difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: Date(), title: "High five! (Normal)", description: "Complete 5 game sessions on Normal difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "High five! (Hard)", description: "Complete 5 game sessions on Hard difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "Keep the ten-sion! (Easy)", description: "Complete 10 game sessions on Easy difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: Date(), title: "Keep the ten-sion! (Normal)", description: "Complete 10 game sessions on Normal difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: Date(), title: "Keep the ten-sion! (Hard)", description: "Complete 10 game sessions on Hard difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "Happy 20th! (Easy)", description: "Complete 20 game sessions on Easy difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "Happy 20th! (Normal)", description: "Complete 20 game sessions on Normal difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "Happy 20th! (Hard)", description: "Complete 20 game sessions on Hard difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "A change in the recipe!", description: "Complete a game session on Custom difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "Take it easy!", description: "Complete a game session with a perfect score on Easy difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "Everything looks normal.", description: "Complete a game session with a perfect score on Normal difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "No hard feelings?", description: "Complete a game session with a perfect score on Hard difficulty.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "Don't give up!", description: "Complete a game session matching less than half of the words.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "Right to silence!", description: "Let the time to answer run out in silence.", badge: nil),
         Achievement(achieved: false, achievementDate: nil, title: "Maybe another time!", description: "Start a game session and then interrupt it before it's over.", badge: nil)]
    
    @IBOutlet weak var achievementsTableView: UITableView!


    @IBOutlet weak var popoverBackgroundView: UIView!
    @IBOutlet weak var popoverView: UIView!
    @IBOutlet weak var popoverTitle: UILabel!
    @IBOutlet weak var popoverDate: UILabel!
    @IBOutlet weak var popoverDescription: UILabel!
    
    @IBAction func tappedOnPopoverBackground(_ sender: UITapGestureRecognizer) {
        popoverDisappears()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Make the table more readeble for wide screens
        self.achievementsTableView.cellLayoutMarginsFollowReadableWidth = true
        
        // Remove infinite blank cells in the table view
        self.achievementsTableView.tableFooterView = UIView (frame: .zero)
        
        // Set SettingViewController as delagete and dataSource for settingTableView
        self.achievementsTableView.delegate = self
        self.achievementsTableView.dataSource = self
        
        // Make rounded corner for popover view
        self.popoverView.cornerRadius = 27
        
        // Initialization for the popover
        self.popoverView.alpha = 0
        self.popoverBackgroundView.alpha = 0
    }
    
    // TABLE SECTION
    // [TABLE] Number of sections in the table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // [TABLE] Number of rows for each section of the table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return achievements.count
        } else{
            return 0
        }
    }
    
    // [TABLE] Create every cell for the table view retrieving the related data from the achievements array
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = achievementsTableView.dequeueReusableCell(withIdentifier: "AchievementCell", for: indexPath)
        let achievement = achievements[indexPath.row]
        cell.textLabel?.text = achievement.title
        if let date = achievement.achievementDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy"
            cell.detailTextLabel?.text = dateFormatter.string(from: date)
        } else {
            cell.detailTextLabel?.text = "-"
        }
    //  cell.showsReorderControl = true
        return cell
    }
        
    // [TABLE] Set what happens when a cell is selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = achievements[indexPath.row].title
        let description = achievements[indexPath.row].description
        let date = achievements[indexPath.row].achievementDate
        let achieved = achievements[indexPath.row].achieved
        popoverAppears(title: title, description: description, date: date, achieved: achieved)
    }
    
    func popoverAppears (title: String, description: String, date: Date?, achieved: Bool){
        self.popoverTitle.text = title
        self.popoverDescription.text = description
        if let date1 = date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy"
            self.popoverDate.text = dateFormatter.string(from: date1)
        } else {
            self.popoverDate.text = "-"
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.popoverView.alpha = 0.95
            self.popoverBackgroundView.alpha = 0.85
            self.popoverView.transform = CGAffineTransform(scaleX: 2, y: 2)
        })
    }
    
    func popoverDisappears(){
        UIView.animate(withDuration: 0.2, animations: {
            self.popoverView.alpha = 0
            self.popoverBackgroundView.alpha = 0
            self.popoverView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        })
    }
}
