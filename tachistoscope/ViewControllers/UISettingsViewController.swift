//
//  UISettingsViewController.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 19/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import SwiftUI

class UISettingsViewController: UIViewController {
    
@IBOutlet weak var difficulty: UISegmentedControl!
    @IBOutlet weak var textSize: UISegmentedControl!
    @IBOutlet weak var textSpeed: UISegmentedControl!
    @IBOutlet weak var textType: UISegmentedControl!
    
    var indexSize: Int = 0
    var indexType: Int = 0
    var indexSpeed: Int = 0
    
    var ssize: CGFloat = 30
    var sizeString: String = ""
    
    var sspeed: Double = 1
    var speedString: String = ""
    
    var gameSettings = GameSettings(indexSize: 0, indexType: 0, indexSpeed: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl.isHidden = true
    }
    
    
    @IBAction func gameDifficulty(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0
        {
            textSize.selectedSegmentIndex = 0
            textSpeed.selectedSegmentIndex = 0
            textType.selectedSegmentIndex = 0
            
            indexType = textType.selectedSegmentIndex
            indexSize = textSize.selectedSegmentIndex
            indexSpeed = textSpeed.selectedSegmentIndex
            
            updateSettings()
            
    }
        else if sender.selectedSegmentIndex == 1 {
            
            textSize.selectedSegmentIndex = 1
            textSpeed.selectedSegmentIndex = 1
            textType.selectedSegmentIndex = 1
            
            indexType = textType.selectedSegmentIndex
            indexSize = textSize.selectedSegmentIndex
            indexSpeed = textSpeed.selectedSegmentIndex
            
            updateSettings()
        }
        else if sender.selectedSegmentIndex == 2 {
            
            textSize.selectedSegmentIndex = 2
            textSpeed.selectedSegmentIndex = 2
            textType.selectedSegmentIndex = 2
            
            indexType = textType.selectedSegmentIndex
            indexSize = textSize.selectedSegmentIndex
            indexSpeed = textSpeed.selectedSegmentIndex
            
            updateSettings()
        }
//        else if textSize.selectedSegmentIndex != textType.selectedSegmentIndex || textSpeed.selectedSegmentIndex != textType.selectedSegmentIndex || textSize.selectedSegmentIndex != textSpeed.selectedSegmentIndex {
//            sender.selectedSegmentIndex = 3
//        }
}
    
    @IBAction func custom1(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex != difficulty.selectedSegmentIndex{
            difficulty.selectedSegmentIndex = 3
            
            indexType = textType.selectedSegmentIndex
            indexSize = textSize.selectedSegmentIndex
            indexSpeed = textSpeed.selectedSegmentIndex
            
            updateSettings()
        }
    }
    @IBAction func custom2(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex != difficulty.selectedSegmentIndex{
            difficulty.selectedSegmentIndex = 3
            
            indexType = textType.selectedSegmentIndex
            indexSize = textSize.selectedSegmentIndex
            indexSpeed = textSpeed.selectedSegmentIndex
            
            updateSettings()
        }
    }
    @IBAction func custom3(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex != difficulty.selectedSegmentIndex{
            difficulty.selectedSegmentIndex = 3
            
            indexType = textType.selectedSegmentIndex
            indexSize = textSize.selectedSegmentIndex
            indexSpeed = textSpeed.selectedSegmentIndex
            
            updateSettings()
        }
    }
    
    @IBOutlet weak var lbl: UILabel!
    @IBAction func tap(_ sender: Any) {
        lbl.font = lbl.font.withSize(ssize)
        lbl.isHidden = false
        lbl.text = gameSettings.setType(type: indexType)
        Timer.scheduledTimer(timeInterval: sspeed, target: self, selector: #selector(showLabel), userInfo: nil, repeats: false)
    }
    
   @objc func showLabel()
    {
        lbl.isHidden = true
    }
    
    
    @objc func updateSettings(){
        
        print(gameSettings.setSize(size: indexSize))
        print(gameSettings.setSpeed(speed: indexSpeed))
        print(gameSettings.setType(type: indexType))
        
        if gameSettings.setSize(size: indexSize) == "small"
        {
            ssize = 20
        }
        else if gameSettings.setSize(size: indexSize) == "normal"
        {
            ssize = 35
        }
        else if gameSettings.setSize(size: indexSize) == "big"
        {
            ssize = 50
        }
        
        if gameSettings.setSpeed(speed: indexSpeed) == "slow"
        {
            sspeed = 2
        }
        else if gameSettings.setSpeed(speed: indexSpeed) == "normal"
        {
            sspeed = 1.2
        }
        else if gameSettings.setSpeed(speed: indexSpeed) == "fast"
        {
            sspeed = 0.5
        }
    }
}



