//
//  UIStatisticsViewController.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 19/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import UIKit

import Foundation
import UIKit


class UIStatisticsViewController: UIViewController{
    
    @IBOutlet weak var gamesLabel: UILabel!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var wrongScoreLabel: UILabel!
    
    var numberGames = 1
    var rightWords = 10
    var wrongWords  = 14
    
    override func viewDidLoad() {

    }
    
    
    func getTotalGames() {
        if (numberGames == 1) {
            gamesLabel.text = " You played \(numberGames) game"
        } else {
            gamesLabel.text = " You played \(numberGames) games"
        }
    }
    
    
    func getRightWords(){
        if (rightWords == 1 ) {
            scoreLabel.text = " You guessed \(rightWords) word in the \(numberGames) game played  "
        } else {
            scoreLabel.text = " You guessed \(rightWords) words in the \(numberGames) games played  "
        }
    }
    
    func getWrongWords(){
        if (wrongWords == 1){
            wrongScoreLabel.text = "You are wrong \(wrongWords) word in the \(numberGames) game played "
        }else {
            wrongScoreLabel.text = "You are wrong \(wrongWords) words in the \(numberGames) games played "
            
        }
    }
}
