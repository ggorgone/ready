//
//  UIProgressViewController.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 19/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import UIKit

class UIProgressViewController: UIViewController {
    @IBOutlet weak var statisticsUIView: UIView!
    @IBOutlet weak var achievementsUIView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.selectedSegmentIndex=1
        
    }
    
    
    
    @IBAction func switchView(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            statisticsUIView.alpha=1.0
            achievementsUIView.alpha=0.0
        }else{
            statisticsUIView.alpha=0.0
            achievementsUIView.alpha=1.0
        }
    }
    
}
