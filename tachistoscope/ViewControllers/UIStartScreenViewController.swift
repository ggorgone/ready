//
//  UIStartScreenViewController.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 19/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import UIKit

class UIStartScreenViewController: UIViewController {
    
    @IBOutlet weak var hiddenButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hiddenButton.sendActions(for: .touchUpInside)
    }
}
