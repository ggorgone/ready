//
//  WordsByComplexity.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 24/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import Foundation

class WordsByComplexity {
        static let simpleWords = ["able", "about",    "above",    "act",    "action",    "add",   "address",    "admit", "adult",    "affect",    "after",    "again",
                     "age",    "air",    "all",    "alone",    "always",    "animal",    "apply",    "area", "arm",   "ask",    "attention",    "artist",
                     "bag",    "ball",    "bank",    "bar",    "buy",    "brother",    "blood",    "body",    "break",    "billion",    "bit",    "black",
                     "call",    "camera",    "car",    "church",    "close",    "case",    "cell",    "cold",    "crime",    "dark", "data",    "day",
                     "dead",    "deal",    "defense",    "degree",    "drop",    "drug",    "dog",    "each",    "eat",    "else",    "event",    "ever",
                     "every",    "eye",    "face",    "fact",    "fall",    "family",    "far",    "five",    "floor",    "fly",    "game",    "garden",
                     "gas",    "go",    "goal",    "good",    "hotel",    "hour",    "house",    "how",    "huge",    "hear",    "heart",    "heat",    "idea",
                     "image",    "itself",    "into",    "job",    "join",    "just",    "keep",    "key",    "kid",    "kitchen",    "law",    "lay",    "lead",
                     "like",    "line",    "list",    "less",    "let",    "music",    "may",    "maybe",    "message",    "method",    "middle",    "myself",
                     "news",    "north",    "only",    "one",    "open",    "official",    "out",    "over",    "page",    "pain",    "parent",    "paper",    "part",
                     "patient",    "push",    "pull",    "phone",    "play",    "quickly",    "race",    "raise",    "range",    "rate",    "rather",    "reach",
                     "read",    "record",    "red",    "reduce",    "realize",    "read",    "ready",    "real",    "shoot",    "short",    "shake",    "share",    "save",
                     "say",    "scene",    "school",    "show",    "side",    "some",    "somebody",    "state",    "station",    "table",    "take",    "talk",    "task",
                     "thank",    "ten",    "tend",    "term",    "total",    "tough",    "toward",    "up",    "upon",    "us",    "use"]

        static let normalWords = ["ability",    "accept",    "according",    "activity",    "audience",    "author",    "administration",    "allow",    "analysis",    "approach",    "available",
                               "behind",    "between",    "certainly",    "character",    "citizen",    "clearly",    "conference",    "century",    "daughter",    "democratic",    "development",    "discussion",
                               "disease",    "despite",    "describe",    "difference",    "discover",    "either",    "enough",    "employee",    "election",    "everything",    "everybody",
                               "experience",    "forward",    "finger",    "generation",    "ground",    "himself",    "hundred",    "husband",    "history",    "identify",    "impact",    "improve",    "institution",
                               "interest",    "international",    "indicate",    "information",    "individual",    "likely",    "material",    "management",    "movement",    "military",    "nearly",    "newspaper",
                               "notice",    "opportunity",    "organization",    "officer",    "operation",    "outside",    "participant",    "performance",    "painting",    "professional",    "particular",
                               "probably",    "population",    "purpose",    "require",    "Republican",    "research",    "resource",    "significant",    "scientist",    "southern",    "structure",
                               "successfully",    "themselves",    "throughout",    "shoulder",    "southern",    "standard",    "strategy",    "technology",    "television",    "thought",    "thousand",    "weapon"]

        static let complexWords = ["actually",    "against",    "agreement",    "ahead",    "although",    "environment",    "environmental",    "especially",

"establish",    "exactly",    "explain",    "financial",    "foreign",    "government",    "growth",    "however",    "investment",
                     "interesting",    "interview",    "involve",    "increase",    "indeed",    "knowledge",    "lawyer",    "majority",    "maintain",
                     "necessary",    "particularly",    "property",    "recently",    "recognize",    "relationship",    "responsibility",    "suddenly",
                     "throughout",    "treatment",    "weapon",    "abnegation",    "abjure",    "adumbrate",    "aggrandize",    "amorphous",    "anachronistic",
                     "circumscribe",    "circumlocution",    "commensurate",    "diaphanous",    "embezzlement",    "extraneous",    "grandiloquent",    "neophyte",
                     "pertinacious",    "preponderance",    "redoubtable",    "recalcitrant",    "surreptitious",    "ubiquitous",    "zephyr"]

}
