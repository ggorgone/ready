//
//  DifficultyEnum.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 18/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

enum Difficulty: Int {
    case EASY = 1;
    case MEDIUM = 2;
    case HARD = 3;    
}
