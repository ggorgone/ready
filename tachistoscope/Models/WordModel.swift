//
//  Word.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 18/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import Foundation

class WordModel {
    let wordText: String;
    
    init(_ wordText: String){
        self.wordText = wordText
    }
    
    func getWordText() -> String{
        self.wordText
    }
}
