//
//  Achievement.swift
//  tachistoscope
//
//  Created by Giovanni Gorgone on 18/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import UIKit;

struct Achievement {
    var achieved: Bool
    var achievementDate: Date?
    let title: String
    let description: String
    let badge: UIImage?
}
