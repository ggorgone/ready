import Foundation

class FileReaderController{
    
    let fileName:String
    
    init(fileName:String){
        self.fileName = fileName
    }
    
    
    func getWords() -> Array<WordModel> {
        
        var words = Array<WordModel>()
        
        if let filepath = Bundle.main.path(forResource: self.fileName, ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                let readWords = contents.components(separatedBy: .newlines)
                for i in 0...readWords.count-2{
                    let word = WordModel(readWords[i])
                    words.append(word)
                }
            } catch {
                print("contents could not be loaded")
            }
        } else {
            print("file not found!")
        }
        
        return words
    }
}
