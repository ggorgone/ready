//
//  StorageController.swift
//  tachistoscope
//
//  Created by Andrea Capone on 24/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import Foundation


class StorageController{
    
    static func setValue(value:String,key:String) -> Void{
        let defaults = UserDefaults.standard
        defaults.set (value,forKey:key)
    }
    
    static func setValue(value:Int,key:String) -> Void{
        let defaults = UserDefaults.standard
        defaults.set(value,forKey:key)
    }
    
    static func getValue(key:String) -> String{
        let defaults = UserDefaults.standard
        if let rightWords = defaults.string(forKey:key){
            return rightWords
        }else {
            return ""
        }
    }
    
    static func getValue(key:String) -> Int{
        let defaults = UserDefaults.standard
        return defaults.integer(forKey: key)

    }
}
