import Foundation

class WordController{

    var words:Array<WordModel>
    var appeared = Array<Bool>()


    init(Difficulty:Difficulty){
        var filename:String
        switch(Difficulty){
            
            case .EASY:
                filename = "EASY"
            break
            
            case .MEDIUM:
                filename = "MEDIUM"
            break
            
            case .HARD:
                filename = "HARD"
            break
                
        }
        
        let reader = FileReaderController(fileName: filename)
        self.words = reader.getWords()
        for _ in words{
            appeared.append(false)
        }
    }
    
    
    func getWords() -> Array<WordModel> {
        self.words
    }


    public func randomWord() -> String{
        var generated = false
        while(!generated){
            let randomWord = Int.random(in: 0...words.endIndex-1)
            if(!appeared[randomWord]){
                appeared[randomWord] = true
                generated = true
                return words[randomWord].getWordText()
            }
        }
    }
}
