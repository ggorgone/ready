//
//  WordSettings.swift
//  tachistoscope
//
//  Created by Adriano Gatto on 24/11/2019.
//  Copyright © 2019 undesigned. All rights reserved.
//

import Foundation

struct GameSettings {
    
    let textSize = ["big", "normal", "small"]
    let textSpeed = ["slow", "normal", "fast"]
    
    var indexSize: Int
    var indexType: Int
    var indexSpeed: Int

    mutating func setSize(size: Int) -> String
    {
        indexSize = size
        return textSize[indexSize]
    }
    
    mutating func setSpeed(speed: Int) -> String
    {
        indexSpeed = speed
        return textSpeed[indexSpeed]
    }
    
    mutating func setType(type: Int) -> String
    {
        if type == 0
        {
            return easyWords.randomElement()!
        }
        else if type == 1
        {
            return mediumWords.randomElement()!
        }
        else
        {
            return hardWords.randomElement()!
        }
        
    }
    
    // Per le parole andare a vedere le costanti statiche in WordsByComplexity
    let easyWords = WordsByComplexity.simpleWords
    let mediumWords = WordsByComplexity.normalWords
    let hardWords = WordsByComplexity.complexWords
}

